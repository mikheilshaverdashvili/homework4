package com.example.homework4

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.profile_layout.view.*

class Profile(private val data:Userdata.Data, private val activity: MainActivity):RecyclerView.Adapter<Profile.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.profile_layout, parent, false))
    }

    override fun getItemCount() = 1;

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind();
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind() {
            Glide.with(activity).load(data.avatar).into(itemView.ProfileImageView)
            itemView.ProfileIdTextView.text = data.id.toString();
            itemView.ProfileNameTextView.text = data.firstName;
            itemView.ProfileLastNameTextView.text = data.lastName;
            itemView.ProfileMailTextView.text = data.email;
        }
    }
}