package com.example.homework4

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_posts_recyclerview_layout.view.*

class datasRecyclerViewAdapter(private val datas:MutableList<Userdata.Data>, private val activity: MainActivity):RecyclerView.Adapter<datasRecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_posts_recyclerview_layout, parent, false))
    }

    override fun getItemCount() = datas.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var data:Userdata.Data
        fun onBind() {
            data = datas[adapterPosition]
            Glide.with(activity).load(data.avatar).into(itemView.imageView)
            itemView.idTextView.text = data.id.toString()
            itemView.fullNameTextView.text = data.firstName + "  " + data.lastName
            itemView.mailTextView.text = data.email
            itemView.setOnClickListener() {
                activity.openProfile(data)
            }
        }
    }
}