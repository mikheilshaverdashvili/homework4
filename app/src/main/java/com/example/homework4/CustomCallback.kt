package com.example.homework4

interface CustomCallback {
    fun onSuccess(result: String){}
    fun onFailure(errorMassage: String){}
}