package com.example.homework4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var adapter:datasRecyclerViewAdapter
    private lateinit var pAdapt:Profile

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getUSers()
    }

    private fun init(data: Userdata) {
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = datasRecyclerViewAdapter(data.data, this)
        recyclerView.adapter = adapter
    }

    public fun openProfile(data: Userdata.Data) {
        recyclerView.layoutManager = LinearLayoutManager(this)
        pAdapt = Profile(data , this)
        recyclerView.adapter = pAdapt
    }

    private fun getUSers() {
        DataLoader.getRequest("users", object: CustomCallback{
            override fun onSuccess(result: String) {
                val data = Gson().fromJson(result, Userdata::class.java)
                init(data);
            }
        })
    }
}
